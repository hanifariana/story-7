from django.test import TestCase, Client
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.http import HttpRequest
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase
from .views import index
from django.apps import apps
from .apps import Story7Config
from django.urls import resolve
import time
import unittest

# Create your tests here.
class UnitTest(TestCase):
    def test_status(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_apps(self):
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('story7').name, 'story7') 

    def test_get(self):
            response = Client().get('/')
            html_kembalian = response.content.decode('utf8')
            self.assertIn("accordion", html_kembalian)
            self.assertIn("activity", html_kembalian)
            self.assertIn("experiences", html_kembalian)   
            self.assertIn("contact me", html_kembalian)
            self.assertIn("achievement", html_kembalian)   
           
    def test_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

# class Story7FuncTest(LiveServerTestCase):
#     def setUp(self):
#         super().setUp()
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         # chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         chrome_options.add_argument('--disable-dev-shm-usage')
#         self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         time.sleep(3)
#         self.browser.get('http://127.0.0.1:8000/')
#         self.browser.maximize_window() #For maximizing window
#         self.browser.implicitly_wait(20)
        
#     def tearDown(self):
#         self.browser.quit()
#         super().tearDown()

#     def test_landing_page(self):
#         self.browser.get(self.live_server_url + '/')
#         self.assertIn('Story 7', self.browser.title)
#         time.sleep(3)

#     def test_display(self):
#         self.browser.get(self.live_server_url + '/')
#         about = self.browser.find_element_by_css_selector('li:nth-child(1) > .accordion')
#         exp = self.browser.find_element_by_css_selector('li:nth-child(2) > .accordion')

#         about.click()
#         exp.click()

#         self.assertIn('Story 7', self.browser.page_source)
#         self.assertIn('PERAK', self.browser.page_source)

#     def test_up_down_button(self):
#         self.browser.get(self.live_server_url + '/')
#         up_exp = self.browser.find_element_by_css_selector('li:nth-child(2) > .myButton:nth-child(1)')
#         down_contact = self.browser.find_element_by_css_selector('li:nth-child(3) > .myButton:nth-child(2)')

#         up_exp.click()

#         down_contact.click()